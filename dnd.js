//Immediately-Invoked Function Expression (IIFE)

(function( $ ) {
        $.fn.simulateDragDrop = function(options) {
                return this.each(function() {
                        new $.simulateDragDrop(this, options);
                });
        };
//        here this means JQuery
        $.simulateDragDrop = function(elem, options) {
              //  elem.draggable({appendTo : options })

                this.options = options;
//                $( "#droppedlist" ).append( $( '<span>' ) );
                this.simulateEvent(elem, options);

        };
        $.extend($.simulateDragDrop.prototype, {
                simulateEvent: function(elem, options) {
                        /*Simulating drag start*/
                        var type = 'dragstart';
                        var event = this.createEvent(type);
                        console.log(elem.textContent)
//                        console.log(event.dataTransfer.data.name)
//                        console.log(event.dataTransfer.data.type)
                        this.dispatchEvent(elem, type, event);


                       /*Simulating dragenter*/
                       type = 'dragenter';
                       var dragenterEvent = this.createEvent(type, {});
                       dragenterEvent.dataTransfer = event.dataTransfer;
                       this.dispatchEvent($(options.dropTarget)[0], type, dragenterEvent);



                        /*Simulating drop*/
                        type = 'drop';
                        var dropEvent = this.createEvent(type, {});
                        dropEvent.dataTransfer = event.dataTransfer;
                        console.log(event.dataTransfer.getData())
//                        dropEvent.dataTransfer.setData(type, event.dataTransfer.getData);
                        this.dispatchEvent($(options.dropTarget)[0], type, dropEvent);



                        /*Simulating drag end*/
                        type = 'dragend';
                        var dragEndEvent = this.createEvent(type, {});
                        dragEndEvent.dataTransfer = event.dataTransfer;
//                        dragEndEvent.dataTransfer.setData(type, event.dataTransfer.getData())
                        this.dispatchEvent(elem, type, dragEndEvent);

                },
                createEvent: function(type) {
                        var event = document.createEvent("CustomEvent");
                        event.initCustomEvent(type, true, true, null);
//                        console.log('egy' + type)
                        event.dataTransfer = {

                                data: { name : "name1",
                                type:""
                                },
                                setData: function(type, val){
                                        this.data.type = val;
                                },
                                getData: function(type){
                                return this.data.type;
                                }
                        };
                        return event;
                },
                dispatchEvent: function(elem, type, event) {
                        if(elem.dispatchEvent) {
                                elem.dispatchEvent(event);
                        }else if( elem.fireEvent ) {
                                elem.fireEvent("on"+type, event);
                        }
                }
        });
})(jQuery);
