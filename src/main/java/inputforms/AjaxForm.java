package inputforms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AjaxForm {
     public String submitAjaxForm(WebDriver driver){
        driver.manage().window().maximize();
        driver.get("https://www.seleniumeasy.com/test/ajax-form-submit-demo.html");
        driver.findElement(By.id("title")).sendKeys("Zsolt");
        driver.findElement(By.id("description")).sendKeys("test automation engineer");
        driver.findElement(By.id("btn-submit")).click();
        WebDriverWait wait = new WebDriverWait(driver,10);
        String s = driver.findElement(By.id("submit-control")).getText();
        wait.until(ExpectedConditions.textToBePresentInElement(driver.findElement(By.id("submit-control")),"Form submited Successfully!"));
        return (s  + driver.findElement(By.id("submit-control")).getText());
    }

}
