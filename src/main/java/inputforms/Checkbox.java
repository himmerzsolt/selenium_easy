package inputforms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;

public class Checkbox {

    public String singleCheckboxDemo(WebDriver driver){
        driver.manage().window().maximize();
        driver.get("https://www.seleniumeasy.com/test/basic-checkbox-demo.html");
        if ( !driver.findElement(By.id("isAgeSelected")).isSelected() )
        {
            driver.findElement(By.id("isAgeSelected")).click();
        }
        return driver.findElement(By.id("txtAge")).getText();
    }

    public String multipleCheckBoxDemo(List<Boolean> selection, WebDriver driver ){
        driver.get("https://www.seleniumeasy.com/test/basic-checkbox-demo.html");
        WebDriverWait wait = new WebDriverWait(driver,10);
        driver.manage().window().maximize();
        List<WebElement> elements = driver.findElements(By.className("cb1-element"));
        int i=0;
        for (WebElement e : elements) {
            if (selection.get(i)){
                wait.until(ExpectedConditions.elementToBeClickable(e)).click();
            }
            i++;
        }
        return driver.findElement(By.id("check1")).getAttribute("value");
    }

    public boolean clickOnCheckAll(WebDriver driver){
        driver.get("https://www.seleniumeasy.com/test/basic-checkbox-demo.html");
        driver.manage().window().maximize();
        List<WebElement> elements = driver.findElements(By.className("cb1-element"));
        driver.findElement(By.id("check1")).click();
        for (WebElement e : elements) {
            if (!(e.isSelected())){
                return false;
            }
        }
        return true;
    }

    public boolean clickOnUncheckAll(WebDriver driver){
        driver.get("https://www.seleniumeasy.com/test/basic-checkbox-demo.html");
        driver.manage().window().maximize();
        List<WebElement> elements = driver.findElements(By.className("cb1-element"));
        for (WebElement e : elements) {
            e.click();
        }
        driver.findElement(By.id("check1")).click();
        for (WebElement r : elements) {
            if (r.isSelected()){
                return false;
            }
        }
        return true;
    }
}
