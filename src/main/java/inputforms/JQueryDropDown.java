package inputforms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class JQueryDropDown {

    public String singleSelect(WebDriver driver, String country){
        driver.manage().window().maximize();
        driver.get("https://www.seleniumeasy.com/test/jquery-dropdown-search-demo.html");
        Select countryList = new Select(driver.findElement(By.id("country")));
        countryList.selectByValue(country);
        return driver.findElement(By.id("select2-country-container")).getText();
    }

    public String multipleSelect(WebDriver driver, List<String> states ){
        driver.manage().window().maximize();
        driver.get("https://www.seleniumeasy.com/test/jquery-dropdown-search-demo.html");
        Select stateList = new Select(driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[2]/div/div[2]/select")));
        for (String s : states){
            stateList.selectByValue(s);
        }
        WebElement ul_element = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[2]/div/div[2]/span/span[1]/span/ul"));
        List<WebElement> li_All = ul_element.findElements(By.tagName("li"));
        StringBuilder result = new StringBuilder();
        for(WebElement element : li_All){
            result.append(element.getText());
        }
        return result.toString();
    }
}
