package inputforms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RadioButton {

    public String radioButton(WebDriver driver){
        driver.get("https://www.seleniumeasy.com/test/basic-radiobutton-demo.html");
        driver.findElement(By.xpath("/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[2]/label[1]/input[1]")).click();
        driver.findElement(By.id("buttoncheck")).click();
        return driver.findElement(By.xpath("//p[@class='radiobutton']")).getText();
    }

    public String multipleRadioButton(WebDriver driver){
        driver.get("https://www.seleniumeasy.com/test/basic-radiobutton-demo.html");
        driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[2]/div[2]/div[1]/label[1]/input")).click();
        driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[2]/div[2]/div[2]/label[1]/input")).click();
        driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[2]/div[2]/button")).click();
        return driver.findElement(By.className("groupradiobutton")).getText();
    }

}
