package inputforms;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import java.util.List;

public class SelectDropDown {

    public String selectOneDayFromList(WebDriver driver, String day){
        driver.get("https://www.seleniumeasy.com/test/basic-select-dropdown-demo.html");
        Select dayList = new Select(driver.findElement(By.id("select-demo")));
        dayList.selectByValue(day);
        return driver.findElement(By.className("selected-value")).getText();
    }

    public String clickOnGetFirstSelected(WebDriver driver, List<String> states) {
        driver.manage().window().maximize();
        Actions actions = new Actions(driver);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        driver.get("https://www.seleniumeasy.com/test/basic-select-dropdown-demo.html");
        WebElement e = driver.findElement(By.id("printMe"));
        js.executeScript("arguments[0].scrollIntoView();", e);
        Select stateList = new Select(driver.findElement(By.id("multi-select")));
        List<WebElement> options =  stateList.getOptions();
        for (String s: states){
                for (WebElement o : options){
                    if (o.getText().equals(s)){
                        actions.keyDown(Keys.LEFT_CONTROL).click(o).build().perform();
                }
            }
        }
        driver.findElement(By.id("printMe")).click();
        return driver.findElement(By.className("getall-selected")).getText();
    }
    public String clickOnGetAllSelected(WebDriver driver, List<String> states){
        driver.manage().window().maximize();
        Actions actions = new Actions(driver);
        driver.get("https://www.seleniumeasy.com/test/basic-select-dropdown-demo.html");
        WebElement e = driver.findElement(By.id("printAll"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", e);

        Select stateList = new Select(driver.findElement(By.id("multi-select")));
        List<WebElement> options =  stateList.getOptions();
        for (String s: states){
            for (WebElement o : options){
                if (o.getText().equals(s)){
                    actions.keyDown(Keys.LEFT_CONTROL).click(o).build().perform();
                }
            }
        }
        e.click();
        return driver.findElement(By.className("getall-selected")).getText();
    }

    public String clickOnFirstSelectedWithoutSelection(WebDriver driver){
        driver.manage().window().maximize();
        driver.get("https://www.seleniumeasy.com/test/basic-select-dropdown-demo.html");
        WebElement e = driver.findElement(By.id("printMe"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", e);
        e.click();
        return driver.findElement(By.className("getall-selected")).getText();
    }
}
