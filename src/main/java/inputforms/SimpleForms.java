package inputforms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SimpleForms {

    public String singleInputField(String inputString, WebDriver driver){
        driver.manage().window().maximize();
        driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
        driver.findElement(By.id("user-message")).sendKeys(inputString);
        driver.findElement(By.className("btn-default")).click();
        WebElement element = driver.findElement(By.id("display"));
        return element.getText();
    }

    public String twoInputFields(String input1, String input2, WebDriver driver){
        driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
        driver.findElement(By.id("sum1")).sendKeys(input1);
        driver.findElement(By.id("sum2")).sendKeys(input2);
        driver.findElement(By.xpath("//button[contains(text(),'Get Total')]")).click();
        WebElement element = driver.findElement(By.id("displayvalue"));
        return element.getText();
    }
}
