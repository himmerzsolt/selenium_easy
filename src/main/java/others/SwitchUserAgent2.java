package others;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.ProfilesIni;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.IOException;

public class SwitchUserAgent2 {
    public static void main(String[] args) throws IOException {
        System.setProperty("webdriver.gecko.driver","C:\\Gecko\\geckodriver-v0.24.0-win64\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        //driver.get("http://demo.guru99.com/test/delete_customer.php");
        ProfilesIni profileIni = new ProfilesIni();
        FirefoxProfile myprofile = profileIni.getProfile("default");
        System.out.println( myprofile.toString());

        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("general.useragent.override", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36 OPR/48.0.2685.52");
        DesiredCapabilities dc=DesiredCapabilities.firefox();
        dc.setCapability(FirefoxDriver.PROFILE, profile);
        WebDriver driver2 = new FirefoxDriver(dc);
        ProfilesIni profileIni2 = new ProfilesIni();
        FirefoxProfile myprofile2 = profileIni2.getProfile("default");
        System.out.println(myprofile2.toString());



    }
}
