package others;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class GridPractice {
    public static void main(String[] args) throws MalformedURLException {
    Practice();
     //   Practice2();

       /* FirefoxProfile fp = new FirefoxProfile();
//      set something on the profile...
        DesiredCapabilities dc = DesiredCapabilities.firefox();
        dc.setCapability(FirefoxDriver.PROFILE, fp);
        WebDriver driver = new RemoteWebDriver(dc);
        driver.get("http://www.google.com");
*/
    }

    public static void Practice() throws MalformedURLException {
          /*  WebDriver driver = new RemoteWebDriver(
                    new URL("http://192.168.178.137:4444/wd/hub"),
                    DesiredCapabilities.chrome());

            driver.get("http://www.google.com");

*/
         //System.setProperty("webdriver.gecko.driver","C:\\Users\\MacBook\\Desktop\\CAPTURE\\Webdrivers\\geckodriver.exec");
         String nodeURL = "http://192.168.178.128:5566/wd/hub";
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setBrowserName("firefox");
        capabilities.setPlatform(Platform.MAC);
        capabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
        WebDriver driver = new RemoteWebDriver(new URL(nodeURL), capabilities);
        driver.get("http://www.google.com");
    }
    public static void Practice2() throws MalformedURLException {

        WebDriver driver = new RemoteWebDriver(
                new URL("http://192.168.178.128:5566/wd/hub"),
                DesiredCapabilities.firefox());

        driver.get("http://www.google.com");
        WebDriver augmentedDriver = new Augmenter().augment(driver);
        File screenshot = ((TakesScreenshot)augmentedDriver).
                getScreenshotAs(OutputType.FILE);
        System.out.println(screenshot.getPath());
        screenshot.renameTo(new File("C:\\picture.png"));
    }
}
