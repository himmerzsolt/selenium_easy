package others;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DynamicData {
    public static void main(String[] args) {
        getRandomUser();
    }

    public static String getRandomUser(){
        System.setProperty("webdriver.gecko.driver","C:\\Gecko\\geckodriver-v0.24.0-win64\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.get("https://www.seleniumeasy.com/test/dynamic-data-loading-demo.html");
        driver.findElement(By.id("save")).click();
        WebDriverWait wait = new WebDriverWait(driver,10);
       // System.out.println(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("loading"))).getText());
        System.out.println(wait.until(ExpectedConditions.textToBePresentInElement(driver.findElement(By.id("loading")),"First Name")));
        String s = driver.findElement(By.id("loading")).getText();

        String s1 = s.replace("First Name :", "");
        String s2 = s1.replace("Last Name :","");
        System.out.println(s2);
        return s;

    }

}
