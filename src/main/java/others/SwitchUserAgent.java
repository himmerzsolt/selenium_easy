package others;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.ProfilesIni;

import java.io.IOException;

public class SwitchUserAgent {
    public static void main(String[] args) throws IOException {

        //Server uses User-Agent string to differentiate between different browsers and devices.
        //Each device + browser combination can have a different name to identify the Browser and its version.
        // Thougth that may not be 100% true in all cases.

        System.setProperty("webdriver.gecko.driver","C:\\Gecko\\geckodriver-v0.24.0-win64\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.get("http://demo.guru99.com/test/delete_customer.php");
        ProfilesIni profileIni = new ProfilesIni();
        FirefoxProfile myprofile = profileIni.getProfile("default");
        System.out.println(myprofile.toString() + myprofile.toJson());


        FirefoxProfile ffp = new FirefoxProfile();
        ffp.setPreference("general.useragent.override", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36 OPR/48.0.2685.52");
        //it does not accept ffp in constructor
        //WebDriver driver2 = new FirefoxDriver(ffp);





    }

}
