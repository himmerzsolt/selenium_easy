package others;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import java.io.File;
import java.io.IOException;

public class DragAndDrop {
    public void dragAndDrop(){


    }
    public void dragAndDrop(WebDriver driver){
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver77_win32\\chromedriver.exe");
        driver.manage().window().maximize();
        driver.get("https://www.seleniumeasy.com/test/drag-and-drop-demo.html");

        String fileContents = null;
        try {
            fileContents = Files.toString(new File("C:\\selenium_easy\\dnd.js"), Charsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript(fileContents+"$('#todrag > span:first-of-type').simulateDragDrop({ dropTarget: '#mydropzone'});");
        js.executeScript(fileContents+"$('#todrag > span:first-of-type').simulateDragDrop({ dropTarget: '#mydropzone'});");
    }
}
