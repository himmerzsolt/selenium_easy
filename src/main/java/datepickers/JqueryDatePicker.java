package datepickers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class JqueryDatePicker {

    public String selectStartAndEndDate(WebDriver driver, String startDate, String endDate){
        driver.manage().window().maximize();
        driver.get("https://www.seleniumeasy.com/test/jquery-date-picker-demo.html");
        driver.findElement(By.cssSelector("#from")).click();
        selectDate(driver,startDate);
        driver.findElement(By.cssSelector("#to")).click();
        selectDate(driver,endDate);
        return driver.findElement(By.cssSelector("#from")).getAttribute("value") + driver.findElement(By.cssSelector("#to")).getAttribute("value");
    }

    public String selectEndAndStartDate(WebDriver driver, String startDate, String endDate){
        driver.manage().window().maximize();
        driver.get("https://www.seleniumeasy.com/test/jquery-date-picker-demo.html");
        driver.findElement(By.cssSelector("#to")).click();
        selectDate(driver,endDate);
        driver.findElement(By.cssSelector("#from")).click();
        selectDate(driver,startDate);
        return driver.findElement(By.cssSelector("#from")).getAttribute("value") + driver.findElement(By.cssSelector("#to")).getAttribute("value");
    }

    private void selectDate(WebDriver driver, String date){
        Select monthList = new Select(driver.findElement(By.className("ui-datepicker-month")));
        String[] dateList = date.split("-");
        monthList.selectByVisibleText(dateList[0]);
        List<WebElement> days = driver.findElements(By.cssSelector(".ui-state-default"));
        for (WebElement e: days){
            if (e.getText().equals(dateList[1])){
                e.click();
                break;
            }
        }
    }
}
