package datepickers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class DatePickerRange {

    public String selectEndDate(WebDriver driver, String date){
        driver.manage().window().maximize();
        driver.get("https://www.seleniumeasy.com/test/bootstrap-date-picker-demo.html");
        driver.findElement(By.xpath("//input[@placeholder='End date']")).click();
        selectDate(driver, date);
        return driver.findElement(By.xpath("//input[@placeholder='Start date']")).getAttribute("value") + driver.findElement(By.xpath("//input[@placeholder='End date']")).getAttribute("value");
    }

    public String selectStartDate(WebDriver driver, String date){
        driver.manage().window().maximize();
        driver.get("https://www.seleniumeasy.com/test/bootstrap-date-picker-demo.html");
        driver.findElement(By.xpath("//input[@placeholder='Start date']")).click();
        selectDate(driver, date);
        return driver.findElement(By.xpath("//input[@placeholder='Start date']")).getAttribute("value") + driver.findElement(By.xpath("//input[@placeholder='End date']")).getAttribute("value");
    }
    public String selectDateRange(WebDriver driver, String startDate, String endDate){
        driver.manage().window().maximize();
        driver.get("https://www.seleniumeasy.com/test/bootstrap-date-picker-demo.html");
        driver.findElement(By.xpath("//input[@placeholder='Start date']")).click();
        selectDate(driver, startDate);
        driver.findElement(By.xpath("//input[@placeholder='End date']")).click();
        selectDate(driver, endDate);
        return driver.findElement(By.xpath("//input[@placeholder='Start date']")).getAttribute("value") + driver.findElement(By.xpath("//input[@placeholder='End date']")).getAttribute("value");
    }
    private void selectDate(WebDriver driver, String date){
        String[] dateList = date.split("/");
        WebElement datePickerDays = driver.findElement(By.cssSelector("div.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-top:nth-child(11) div.datepicker-days table.table-condensed thead:nth-child(1) tr:nth-child(2) > th.datepicker-switch"));
        datePickerDays.click();
        WebElement datePickerMonths = driver.findElement(By.cssSelector("div.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-top:nth-child(11) div.datepicker-months table.table-condensed thead:nth-child(1) tr:nth-child(2) > th.datepicker-switch"));
        datePickerMonths.click();
        WebElement datePickerYears = driver.findElement(By.cssSelector("div.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-top:nth-child(11) div.datepicker-years table.table-condensed thead:nth-child(1) tr:nth-child(2) > th.datepicker-switch"));
        datePickerYears.click();
        selectYear(dateList ,driver);
    }
    private void selectYear(String[] dateList, WebDriver driver){
        WebElement prev = driver.findElement(By.xpath("/html/body/div[3]/div[3]/table/thead/tr[2]/th[1]"));
//        WebElement prev = driver.findElement(By.cssSelector("body > div.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-top > div.datepicker-years > table > thead > tr:nth-child(2) > th.prev"));
        WebElement next = driver.findElement(By.xpath("/html/body/div[3]/div[3]/table/thead/tr[2]/th[3]"));
//        WebElement next = driver.findElement(By.cssSelector("body > div.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-top > div.datepicker-years > table > thead > tr:nth-child(2) > th.next"));
        List<WebElement> years = driver.findElements(By.cssSelector(".year:not(.new):not(.old)"));
        int x = Integer.parseInt(dateList[0]);
        here:
        do {
            if(x>Integer.parseInt(years.get(9).getText())){
                next.click();
            }
            else if (x<Integer.parseInt(years.get(0).getText())){
                prev.click();
            } else {
                for (WebElement y : years) {
                    if (x == Integer.parseInt(y.getText())) {
                        y.click();
                        selectMonth(dateList, driver);
                        break here;
                    }
                }
            }
            years = driver.findElements(By.cssSelector(".year:not(.new):not(.old)"));
        } while(true);
    }
    private void selectMonth(String[] dateList, WebDriver driver) {
        List<WebElement> months = driver.findElements(By.cssSelector(".month"));
        for (WebElement m : months) {
            if (m.getText().equals(dateList[1])) {
                m.click();
                selectDay(dateList, driver);
                break;
            }
        }
    }
    private void selectDay(String[] dateList, WebDriver driver){
        List<WebElement> days = driver.findElements(By.cssSelector(".day:not(.new):not(.old)"));
        for (WebElement d : days) {
            if (d.getText().equals(dateList[2])) {
                if (d.getAttribute("class").contains("disabled")){
                    break;
                } else{
                    d.click();
                    break;
                }
            }
        }
    }
}
