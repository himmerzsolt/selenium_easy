package datepickers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class DatePicker {

    private WebElement formControl;

    public String selectDate(WebDriver driver, String date){
        firstClick(driver);
        String[] dateList = date.split("/");
        WebElement datePickerDays = driver.findElement(By.cssSelector("div.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-top:nth-child(11) div.datepicker-days table.table-condensed thead:nth-child(1) tr:nth-child(2) > th.datepicker-switch"));
        datePickerDays.click();
        WebElement datePickerMonths = driver.findElement(By.cssSelector("div.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-top:nth-child(11) div.datepicker-months table.table-condensed thead:nth-child(1) tr:nth-child(2) > th.datepicker-switch"));
        datePickerMonths.click();
        WebElement datePickerYears = driver.findElement(By.cssSelector("div.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-top:nth-child(11) div.datepicker-years table.table-condensed thead:nth-child(1) tr:nth-child(2) > th.datepicker-switch"));
        datePickerYears.click();
        WebElement datePickerDecades = driver.findElement(By.cssSelector("div.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-top:nth-child(11) div.datepicker-decades table.table-condensed thead:nth-child(1) tr:nth-child(2) > th.datepicker-switch"));
        datePickerDecades.click();
        selectCentury(dateList ,driver);
        return formControl.getAttribute("value");
    }
    private void selectCentury(String[] dateList, WebDriver driver){
        List<WebElement> centuries = driver.findElements(By.cssSelector(".century:not(.new):not(.old)"));
        WebElement prev;
        int x = Integer.parseInt(dateList[0]);
        int y;

        here:
        do {
                y = x - x % 100;
                    for (WebElement c : centuries) {
                        if (y==Integer.parseInt(c.getText())) {
                            if (c.getAttribute("class").contains("disabled")){
                                break here;
                            }
                            else {
                                c.click();
                                selectDecade(dateList, driver);
                                break here;
                            }
                        }
                    }
            prev = driver.findElement(By.xpath("/html/body/div[3]/div[5]/table/thead/tr[2]/th[1]"));
            prev.click();
            centuries = driver.findElements(By.cssSelector(".century:not(.disabled):not(.new):not(.old)"));
        } while(true);
    }

    private void selectDecade(String[] dateList, WebDriver driver){
        WebElement prev;
        List<WebElement> decades = driver.findElements(By.cssSelector(".decade:not(.new):not(.old)"));
        int x = Integer.parseInt(dateList[0]);
        int y;

        here:
        do {
                y = x - x % 10;
                    for (WebElement d : decades) {
                        if (y==Integer.parseInt(d.getText())) {
                            if (d.getAttribute("class").contains("disabled")){
                                break here;
                            }
                            else {
                                d.click();
                                selectYear(dateList, driver);
                                break here;
                            }
                        }
                    }
            prev = driver.findElement(By.xpath("/html/body/div[3]/div[4]/table/thead/tr[2]/th[1]"));
                    prev.click();
            decades = driver.findElements(By.cssSelector(".decade:not(.old):not(.new)"));
        } while(true);
    }

    private void selectYear(String[] dateList, WebDriver driver) {
        WebElement prev;
        int x = Integer.parseInt(dateList[0]);

        List<WebElement> years = driver.findElements(By.cssSelector(".year:not(.new):not(.old)"));
        here:
        do {
            for (WebElement y : years) {
                if (x==Integer.parseInt(y.getText())) {
                    if (y.getAttribute("class").contains("disabled")){
                        break here;
                    }
                    else {
                        y.click();
                        selectMonth(dateList, driver);
                        break here;
                    }
                }
            }
            prev = driver.findElement(By.xpath("/html/body/div[3]/div[3]/table/thead/tr[2]/th[1]"));
            prev.click();
            years = driver.findElements(By.cssSelector(".year"));
        } while(true);
    }

    private void selectMonth(String[] dateList, WebDriver driver){
        List<WebElement> months = driver.findElements(By.cssSelector(".month"));
            for (WebElement m : months) {
                if (m.getText().equals(dateList[1])) {
                    if(m.getAttribute("class").contains("disabled")){
                        break;
                    } else {
                        m.click();
                        selectDay(dateList,driver);
                        break;
                    }
                }
            }
        }

    private void selectDay(String[] dateList, WebDriver driver){
        List<WebElement> days = driver.findElements(By.cssSelector(".day:not(.new):not(.old)"));
            for (WebElement d : days) {
                if (d.getText().equals(dateList[2])) {
                    if (d.getAttribute("class").contains("disabled")){
                        break;
                    } else{
                        d.click();
                        break;
                    }
                }
            }
        }
     public String clickTodayButton(WebDriver driver){
         firstClick(driver);
         WebElement today = driver.findElement(By.xpath("//div[@class='datepicker-days']//th[@class='today'][contains(text(),'Today')]"));
         today.click();
         return formControl.getAttribute("value");
     }

     public static String convertTodayDateToDatePickerOutputFormat(){
         String datePickerDatePattern = "dd/MM/yyyy";
         DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(datePickerDatePattern);
         System.out.println(dateFormatter.format(LocalDate.now()));
         return dateFormatter.format(LocalDate.now());
     }
     public String clickClear(WebDriver driver){
         firstClick(driver);
         driver.findElement(By.xpath("//div[@class='datepicker-days']//th[@class='clear'][contains(text(),'Clear')]")).click();
         return formControl.getAttribute("value");
     }
     private void firstClick(WebDriver driver){
         driver.manage().window().maximize();
         driver.get("https://www.seleniumeasy.com/test/bootstrap-date-picker-demo.html");
         formControl = driver.findElement(By.xpath("//input[@placeholder='dd/mm/yyyy']"));
         formControl.click();
     }
}
