package others;

import environment.RunEnvironment;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;

public class DragAndDropTest {
    private DragAndDrop underTest;
    WebDriver driver;

    @BeforeEach
    public void setUp () {
        driver =  RunEnvironment.getWebDriver();
        underTest = new DragAndDrop();
    }

    @Test
    public void returnTrue(){
        underTest.dragAndDrop(driver);
    }
    @AfterEach
    public void tearDown(){
        driver.close();
    }
}
