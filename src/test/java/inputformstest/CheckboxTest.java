package inputformstest;

import environment.RunEnvironment;
import inputforms.Checkbox;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class CheckboxTest {
    private Checkbox underTest;
    WebDriver driver;

    @BeforeEach
    public void setUp () {
        driver =  RunEnvironment.getWebDriver();
        underTest = new Checkbox();
    }

    @Test
    public void shouldReturnTrueIfCheckboxIsSelected(){
         assertTrue(underTest.singleCheckboxDemo(driver).equals("Success - Check box is checked"));
    }

    @Test
    public void shouldReturnTrueAllCheckboxAreSelected(){
        List<Boolean> selection = new ArrayList<Boolean>();
        selection.add(Boolean.TRUE);
        selection.add(Boolean.TRUE);
        selection.add(Boolean.TRUE);
        selection.add(Boolean.TRUE);
        assertTrue(underTest.multipleCheckBoxDemo(selection,driver).equals("Uncheck All"));
    }


    @Test
    public void shouldReturnTrueIfNotAllCheckboxIsSelected(){
        List<Boolean> selection = new ArrayList<Boolean>();
        selection.add(Boolean.FALSE);
        selection.add(Boolean.TRUE);
        selection.add(Boolean.TRUE);
        selection.add(Boolean.FALSE);
        assertTrue(underTest.multipleCheckBoxDemo(selection, driver).equals("Check All"));
    }

    @Test
    public void shouldReturnTrueIfAllCheckboxAreTicked(){
        assertTrue(underTest.clickOnCheckAll(driver));
    }
    @Test
    public void shouldReturnTrueIfNoCheckboxIsTicked(){
        assertTrue(underTest.clickOnUncheckAll(driver));
    }


    @AfterEach
    public void tearDown(){
        driver.quit();
    }

}
