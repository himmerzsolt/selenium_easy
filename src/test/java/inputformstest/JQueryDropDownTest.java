package inputformstest;

import environment.RunEnvironment;
import inputforms.JQueryDropDown;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import java.util.Arrays;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.stringContainsInOrder;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class JQueryDropDownTest {
    private JQueryDropDown underTest;
    WebDriver driver;

    @BeforeEach
    public void setUp () {
        driver =  RunEnvironment.getWebDriver();
        underTest = new JQueryDropDown();
    }
    //@Disabled
    @Test
    public void returnTrueIfTextMatchesInputText(){
        assertTrue(underTest.singleSelect(driver,"Denmark").equals("Denmark"));
    }
    //@Disabled
    @Test
    public void returnTrueIfTextMatchesInputTextMultipleSelection(){
        assertThat(underTest.multipleSelect(driver, Arrays.asList("CO","DE", "MD")), stringContainsInOrder(Arrays.asList("Colorado","Delaware", "Maryland")));
    }
    @AfterEach
    public void tearDown(){
        driver.quit();
    }
}
