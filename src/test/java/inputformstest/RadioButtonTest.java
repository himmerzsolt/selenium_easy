package inputformstest;

import environment.RunEnvironment;
import inputforms.RadioButton;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.stringContainsInOrder;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class RadioButtonTest {
    private RadioButton underTest;
    private WebDriver driver;

    @BeforeEach
    public void setUp(){
        driver =  RunEnvironment.getWebDriver();
        underTest = new RadioButton();
    }
    @Disabled
    @Test
    public void shouldReturnTrueIfRadioButtonMaleIsSelected(){
       assertTrue(underTest.radioButton(driver).equals("Radio button 'Male' is checked"));
    }
    @Disabled
    @Test
    public void shouldReturnTrueIfMaleCheckedAnd05AgeGroupIsSelected(){
        assertThat(underTest.multipleRadioButton(driver),stringContainsInOrder(Arrays.asList("Sex : Male", "Age group: 0 - 5")));
    }

    @AfterEach
    public void tearDown(){
        driver.quit();
    }

}
