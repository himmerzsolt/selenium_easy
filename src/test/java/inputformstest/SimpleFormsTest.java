package inputformstest;

import environment.RunEnvironment;
import inputforms.SimpleForms;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SimpleFormsTest {

    private SimpleForms underTest;
    WebDriver driver;

    @BeforeEach
    public void setUp () {
        driver =  RunEnvironment.getWebDriver();
        underTest = new SimpleForms();
    }

    @Test
    public void returnTrueIfInputStringMatchesOutputString(){
        assertTrue(underTest.singleInputField("Selenium_test", driver).equals("Selenium_test"));
    }
    @Test
    public void shouldReturnTrueIfOutputStringMatchesResult(){
        String input1 = "12";
        String input2 = "11";
        String result = "23";
        assertTrue(underTest.twoInputFields(input1,input2,driver).equals(result));
    }
    @AfterEach
    public void tearDown(){
         driver.close();
    }
}
