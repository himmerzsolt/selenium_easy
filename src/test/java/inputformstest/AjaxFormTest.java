package inputformstest;

import environment.RunEnvironment;
import inputforms.AjaxForm;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.stringContainsInOrder;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import java.util.Arrays;

public class AjaxFormTest {
    public WebDriver driver;
    public AjaxForm underTest;

    @BeforeEach
    public void setup(){
        driver =  RunEnvironment.getWebDriver();
        underTest = new AjaxForm();
    }
    @Test
    public void ajaxTest(){
        assertThat(underTest.submitAjaxForm(driver), stringContainsInOrder(Arrays.asList("Ajax Request is Processing!","Form submited Successfully!")));

    }
    @AfterEach
    public void tearDown(){
        driver.quit();
    }
}
