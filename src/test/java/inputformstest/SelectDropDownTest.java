package inputformstest;

import environment.RunEnvironment;
import inputforms.SelectDropDown;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.stringContainsInOrder;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import java.util.Arrays;

public class SelectDropDownTest {
    private SelectDropDown underTest;
    WebDriver driver;

    @BeforeEach
    public void setUp () {
        driver =  RunEnvironment.getWebDriver();
        underTest = new SelectDropDown();
    }
    @Disabled
    @Test
    public void returnTrueIfSelectedDayIsReturned(){
        assertTrue(underTest.selectOneDayFromList(driver,"Wednesday").contains("Wednesday"));
    }
    @Disabled
    @Test
    public void returnTrueFirstSelectedIsInTheTextBox(){
        assertTrue(underTest.clickOnGetFirstSelected(driver,Arrays.asList("New York", "Florida", "Washington")).contains("New York"));
        assertTrue(underTest.clickOnGetFirstSelected(driver,Arrays.asList("New Jersey", "New York", "Washington")).contains("New Jersey"));
        assertTrue(underTest.clickOnGetFirstSelected(driver,Arrays.asList("Florida", "New York", "Washington", "New Jersey")).contains("Florida"));

    }
    @Disabled
    @Test
    public void returnTrueIfTextBoxContainsAllSelectedStates(){
        assertThat(underTest.clickOnGetAllSelected(driver,Arrays.asList("Ohio", "Texas")), stringContainsInOrder(Arrays.asList("Ohio", "Texas")));
        assertThat(underTest.clickOnGetAllSelected(driver, Arrays.asList("Ohio", "Texas", "New York")), stringContainsInOrder(Arrays.asList("Ohio", "Texas", "New York")));
    }
    @Disabled
    @Test
    public void returnTrueIfNothingIsSelectedAndTextBoxContainsUndefined(){
        assertTrue(underTest.clickOnFirstSelectedWithoutSelection(driver).contains("undefined"));
    }
    @AfterEach
    public void tearDown(){

        driver.quit();
    }
}
