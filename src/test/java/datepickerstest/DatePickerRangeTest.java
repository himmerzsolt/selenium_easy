package datepickerstest;

import datepickers.DatePickerRange;
import environment.RunEnvironment;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.WebDriver;
import static org.hamcrest.MatcherAssert.assertThat;
import java.util.Arrays;
import static org.hamcrest.Matchers.*;

class DatePickerRangeTest {
    private DatePickerRange underTest;
    private WebDriver driver;

    @BeforeEach
    void setUp() {
        driver =  RunEnvironment.getWebDriver();
        underTest = new DatePickerRange();
    }

    @DisplayName("check random dates after 1000 AC")
    @ParameterizedTest(name = "{index} => start date {0}, end date {1}")
    @CsvSource({"2008/Dec/31,2009/Jan/1,31/12/200801/01/2009",
                "1999/Dec/31,2000/Jan/1,31/12/199901/01/2000",
                "2002/Jan/31,2002/Feb/1,31/01/200201/02/2002",
                "2003/Apr/30,2003/May/1,30/04/200301/05/2003",
                "2013/Mar/15,2015/Jun/1,15/03/201301/06/2015"})
    void checkRandomDatesAfter1000AC(String startDate, String endDate, String expected){
        assertThat(underTest.selectDateRange(driver,startDate, endDate),containsString(expected));
    }

    @DisplayName("check dates between 100 and 1000 AC")
    @ParameterizedTest(name = "{index} => start date {0}, end date {1}")
    @CsvSource({"556/Sep/6,899/Nov/26,06/09/55626/11/899",
                "456/Oct/3,901/Nov/19,03/10/45619/11/901"
    })
    void checkDatesBetween100And1000AC(String startDate, String endDate, String expected){
        assertThat(underTest.selectDateRange(driver,startDate, endDate), containsString(expected));

    }

    @DisplayName("check dates between 0 and 100")
    @ParameterizedTest(name = "{index} => start date {0}, end date {1}")
    @CsvSource({"26/Nov/9,99/Dec/31",
                "1/Dec/31,33/Dec/31"
    })
    void checkDatesBetween0And100(String startDate, String endDate){
        //Days in years between 0 and 99 are disabled so we expect empty string
        assertThat(underTest.selectDateRange(driver,startDate, endDate), isEmptyString());
    }


    @DisplayName("check Sundays")
    @ParameterizedTest(name = "{index} => start date {0}, end date {1}")
    @CsvSource({"1851/Mar/9,1900/Feb/11"})
    void checkSundays(String startDate, String endDate){
        //Sundays are disabled, so we expect empty string
        assertThat(underTest.selectDateRange(driver,startDate, endDate), isEmptyString());
    }

    @DisplayName("check future dates")
    @ParameterizedTest(name = "{index} => start date {0}, end date {1}")
    @CsvSource({"2100/Oct/11,2111/Oct/12,11/10/210012/10/2111",
                "2019/Dec/31,2020/Oct/22,31/12/201922/10/2020"
    })
    void checkFutureDates(String startDate, String endDate,String expected){
        assertThat(underTest.selectDateRange(driver,startDate, endDate),containsString(expected)) ;
    }

    @DisplayName("check dates before christ")
    @Test
    void checkDatesBeforeChrist(){
        assertThat(underTest.selectDateRange(driver,"-2019/Oct/30", "-1001/Aug/3"), stringContainsInOrder(Arrays.asList("30/10/-2019", "03/08/-1001")));
    }


    @DisplayName("start date should be less than end date")
    @ParameterizedTest(name = "{index} => start date {0}, end date {1}")
    @CsvSource({"2019/Aug/7,2019/Jul/8,08/07/201908/07/2019",
            "2019/Jul/8,2015/Mar/6,06/03/201506/03/2015"
    })
    void startDateShouldBeLessThanEndDate(String startDate, String endDate,String expected){
        //if we try to select an end date that is earlier than start date, both fields will show the end date
        // because start date is adjusted to be equal to end date
        assertThat(underTest.selectDateRange(driver,startDate,endDate), containsString(expected));
    }

    @DisplayName("selected date is displayed in both fields")
    @ParameterizedTest(name = "{index} => start date {0}, end date {1}")
    @CsvSource({"2019/Jul/8,08/07/201908/07/2019",
            "2015/Mar/6,06/03/201506/03/2015"
    })
    void selectedDateIsDisplayedInBothFields(String startDate, String expected){
        assertThat(underTest.selectStartDate(driver, startDate), containsString(expected));
    }


    @AfterEach
    void tearDown(){
        driver.quit();
    }
}
