package datepickerstest;

import datepickers.JqueryDatePicker;
import environment.RunEnvironment;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import java.util.Arrays;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.stringContainsInOrder;
import static org.junit.jupiter.api.Assertions.assertTrue;

class JqueryDatePickerTest {
    private JqueryDatePicker underTest;
    private WebDriver driver;

    @BeforeEach
    void setUp () {
        driver =  RunEnvironment.getWebDriver();
        underTest = new JqueryDatePicker();
    }
//    @Ignore
    @Test
    void returnTrue(){
        //year cannot be selected only month and day
       assertThat(underTest.selectStartAndEndDate(driver,"Oct-12", "Oct-17"), stringContainsInOrder(Arrays.asList("10/12/2019","10/17/2019")));
       // Start Date and the dates before the Start Date disabled in the End Date so we expect that only start date is shown
        //i.e. start date should be less then end date
       assertTrue(underTest.selectStartAndEndDate(driver,"Oct-17", "Oct-12").equals("10/17/2019"));
       //End Date and the dates after the End Date disabled in the Start Date so we expect that only end date is shown
        assertTrue(underTest.selectEndAndStartDate(driver,"Oct-21", "Oct-20").equals("10/20/2019"));
    }
    @AfterEach
    void tearDown(){
        driver.quit();
    }
}
