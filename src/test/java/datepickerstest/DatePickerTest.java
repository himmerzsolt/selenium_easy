package datepickerstest;

import datepickers.DatePicker;
import environment.RunEnvironment;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.WebDriver;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.text.IsEmptyString.isEmptyString;
import static org.junit.jupiter.api.Assertions.assertTrue;

    class DatePickerTest {
    private DatePicker underTest;
    private WebDriver driver;

    @BeforeEach
    void setUp () {
        driver =  RunEnvironment.getWebDriver();
        underTest = new DatePicker();
    }

    @DisplayName("check random dates after 1000AC")
    @ParameterizedTest(name = "{index} => date {0}")
    @CsvSource({"2008/Dec/31,31/12/2008",
                "2009/Jan/1,01/01/2009",
                "1999/Dec/31,31/12/1999",
                "2000/Jan/1,01/01/2000",
                "2002/Feb/1,01/02/2002",
                "2013/Mar/15,15/03/2013"
    })
    void checkRandomDatesAfter1000AC(String date, String expected){
        assertTrue(underTest.selectDate(driver, date).equals(expected));
    }

    @DisplayName("check dates between 100 and 1000AC")
    @ParameterizedTest(name = "{index} => date {0}")
    @CsvSource({"900/Aug/26,26/08/900",
                "899/Nov/26,26/11/899",
                "556/Sep/6,06/09/556"
    })
     void checkDatesBetween100And1000AC(String date, String expected){
       assertTrue(underTest.selectDate(driver, date).equals(expected));
    }


    @DisplayName("check dates between 0 And 100")
    @ParameterizedTest(name = "{index} => date {0}")
    @CsvSource({"26/Nov/9",
                "99/Dec/31",
                "1/Dec/31"
    })
    void checkDatesBetween0And100(String date){
        //Days in years between 0 and 99 are disabled so we expect empty string
        assertThat(underTest.selectDate(driver, date), isEmptyString());
    }

    @DisplayName("check Sundays")
    @ParameterizedTest(name = "{index} => date {0}")
    @CsvSource({"1900/Feb/11",
            "1851/Mar/9",
            "1995/Jul/23"
    })
    void checkSundays(String date){
        //Sundays are disabled, so we expect empty string
        assertThat(underTest.selectDate(driver, date),isEmptyString());
    }
    @Disabled
    @Test
    void checkTodayButton(){
        assertTrue(underTest.clickTodayButton(driver).equals(DatePicker.convertTodayDateToDatePickerOutputFormat()));
    }
    @Disabled
    @Test
    void checkClearButton(){
        assertTrue(underTest.selectDate(driver,"2019/Oct/11").equals("11/10/2019"));
        assertTrue(underTest.clickClear(driver).equals(""));
    }

    @DisplayName("check future dates")
    @ParameterizedTest(name = "{index} => date {0}")
    @CsvSource({"2100/Oct/11",
                "2022/Oct/11",
                "2020/Oct/22"
    })
    void checkFutureDates(String date){
        //future dates are disabled we expect empty string
       assertThat(underTest.selectDate(driver,date),isEmptyString());
    }

    @DisplayName("check dates before christ")
    @ParameterizedTest(name = "{index} => date {0}")
    @CsvSource({"-2019/Oct/30,30/10/-2019",
            "-9001/Aug/3,03/08/-9001"
    })
    void checkDatesBeforeChrist(String date, String expected){
        assertTrue(underTest.selectDate(driver,date).equals(expected));
    }
    @AfterEach
    void tearDown(){
        driver.quit();
    }
}
